=======================
Paste Image Into Finder
=======================

.. _pngpaste: https://github.com/jcsalterego/pngpaste

We can use pngpaste_ to paste images from the clipboard into a directory:

.. code-block::

   $ pngpaste "$(date +'%Y-%m-%d-%H-%M-%S'-my-image.png)"


The Bash Script
---------------

Let's come up with a shell script and integrate it with macOS Quick
Actions service.

.. code-block:: bash

   osascript -e 'set formattedDate to (do shell script "date +'%Y-%m-%dat%H-%M-%S%p'")' \
          -e 'tell application "Finder" to set thePath to the quoted form of (POSIX path of (insertion location as alias) & formattedDate & ".png")' \
          -e 'if ((clipboard info) as string) contains TIFF picture then do shell script "/opt/homebrew/bin/pngpaste " & thePath' \
          -e 'if ((clipboard info) as string) does not contain TIFF picture then tell application "System Events" to keystroke "v" using control down'

Using Quick Actions
-------------------

Open “Automator” → “Quick Action” then search for “Run Shell Script” and
select it. Then, check:

- “Workflow receives” **no input**
- “in” ``Finder.app``

Below that, for the script, check:

- “Shell” ``/bin/bash`` (or something like ``/usr/local/bin/bash`` if
  you have a more recent version, perhaps installed with brew)
- “Pass input” **to stdin**.

.. image:: .
   :alt: macos quick action paste image with pngpaste

Add Keyboard Shortcut
---------------------

Open Keyboard settings → Shortcuts → Services → Paste Image and set
the desired shortcut.

**NOTE**: I tried ``Cmd+p`` and ``Shift+Cmd+p`` and it simply would
not work. It turns out if some keyboard shortcut already exists with
that combination it simply won't warn you, but it will just not
work. I tried ``Shift+Cmd+v`` and then it worked.

.. image:: ./finder-paste-image.assets/2021-08-08-11-46-26-my-image.png
   :alt: add keyboard shortcut for the service

.. note::

   A folder can just be entered into. See this screenshot:


Then, with a folder selected, press ``Shift+Cmd+v`` to paste the image
into the selected directory.

References
----------

- https://github.com/jcsalterego/pngpaste
- http://hints.macworld.com/article.php?story=20100509134904820



